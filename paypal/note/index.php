<?php


include '../../../config.php';


if( $transaction_id = trim($_GET['id']) ){
    
    $response = get_transaction_details($transaction_id);

    foreach ($response as $key => $value) {

        if( $key == 'L_SHORTMESSAGE0' ){
            echo 'NULL';
            break;

        } else if($key == 'NOTE' ){

            if( empty($value) ){
                echo 'NO NOTE';

            } else {
                echo $value;
            }
      
        }

    }

}



function get_transaction_details( $transaction_id ) {
    
    $api_request = 'USER=' . api_user
                .  '&PWD=' . api_pass
                .  '&SIGNATURE=' . api_signature
                .  '&VERSION=76.0'
                .  '&METHOD=GetTransactionDetails'
                .  '&TransactionID=' . $transaction_id;

    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, 'https://api-3t.paypal.com/nvp' );
    curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_POST, 1 );

    $postFields = curl_setopt( $ch, CURLOPT_POSTFIELDS, $api_request );
    $response = curl_exec( $ch );

    if( ! $response ){
        echo "ER";
        die();
        // die( 'Calling PayPal to change_subscription_status failed: ' . curl_error( $ch ) . '(' . curl_errno( $ch ) . ')' );
    }

    curl_close( $ch );

    parse_str( $response, $parsed_response );

    return $parsed_response;
    
}







