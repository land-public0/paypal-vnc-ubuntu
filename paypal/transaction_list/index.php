<?php

include '../../../config.php';

if(! $h = $_GET['h'] ){
    $h = 48;
}

$tdb = date('U') - 3600* $h;
$tmr = date('U') + 3600* 24;

$info = 'USER='.api_user
        .'&PWD='.api_pass
        .'&SIGNATURE='.api_signature
        .'&METHOD=TransactionSearch'
        // .'&TRANSACTIONCLASS=REC/**/EIVED'
        .'&STARTDATE='.date('Y-m-d',$tdb).'T'.date('H:i:s',$tdb).'Z'
        .'&ENDDATE='.date('Y-m-d',$tmr).'T'.date('H:i:s',$tmr).'Z'
        .'&VERSION=94';

$curl = curl_init('https://api-3t.paypal.com/nvp');
curl_setopt($curl, CURLOPT_FAILONERROR, true);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($curl, CURLOPT_POSTFIELDS,  $info);
curl_setopt($curl, CURLOPT_HEADER, 0);
curl_setopt($curl, CURLOPT_POST, 1);

$result = curl_exec($curl);

$result = explode("&", $result);

foreach($result as $value){
    $value = explode("=", $value);
    $temp[$value[0]] = $value[1];
}


foreach($temp as $k=>$v){
    $i++;
    preg_match('#^(.*?)([0-9]+)$#is',$k,$str);
    $num=$str[2];
    $key=preg_replace('#^[A-z]_#','',$str[1]);
    if($key!=''){
        $new[$num][$key]=urldecode($v);
    }

}

foreach( $new as $tnx ){
    if( ($tnx['AMT']<0 and $tnx['TYPE']=='Authorization')  or  in_array( $tnx['TYPE'], array('Payment', 'Purchase', 'Recurring Payment') ) ){
        if( $tnx['EMAIL'] != '' ){
            $tnx_arr[] = array( 
                'TYPE' => $tnx['TYPE'],
                'TIMESTAMP' => $tnx['TIMESTAMP'], 
                'EMAIL' => $tnx['EMAIL'], 
                'TRANSACTIONID' => $tnx['TRANSACTIONID'], 
                'AMT' => $tnx['AMT'], 
                'FEE' => $tnx['FEEAMT'],
                'CURRENCYCODE' => $tnx['CURRENCYCODE'] );
        }
    }
}

echo json_encode($tnx_arr);










