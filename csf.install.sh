#!/bin/bash

if [ -d /etc/csf ]; then
    echo "already installed"    
    
else

    apt-get update -y
    apt-get install wget -y

    cd /usr/src
    rm -fv csf.tgz
    wget https://download.configserver.com/csf.tgz
    tar -xzf csf.tgz
    cd csf
    sh install.sh
    
    rm -rf /etc/csf/csf.conf
    cp /var/www/html/csf.conf /etc/csf/csf.conf
    
    /usr/sbin/csf -r
    /sbin/reboot
    
fi

